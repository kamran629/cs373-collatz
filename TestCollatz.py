#!/usr/bin/env python3
"""
test file
"""

# ----------------------
# collatz/TestCollatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ----------------------

# -------
# imports
# -------

from io       import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz(TestCase):
    """
    test cases
    """
    # ----
    # read
    # ----

    def test_read(self):
        """
        read tests
        """
        store_val = "1 10\n"
        i, j = collatz_read(store_val)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        test eval1
        """
        val = collatz_eval(1, 10)
        self.assertEqual(val, 20)

    def test_eval_2(self):
        """
        test eval 2
        """
        value = collatz_eval(100, 200)
        self.assertEqual(value, 125)

    def test_eval_3(self):
        """
        test eval 3
        """
        value_stored = collatz_eval(201, 210)
        self.assertEqual(value_stored, 89)

    def test_eval_4(self):
        """
        test eval 4
        """
        values_stores = collatz_eval(900, 1000)
        self.assertEqual(values_stores, 174)

    # -----
    # print
    # -----

    def test_print(self):
        """
        test print
        """
        var_w = StringIO()
        collatz_print(var_w, 1, 10, 20)
        self.assertEqual(var_w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        test solve
        """
        var_r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        word = StringIO()
        collatz_solve(var_r, word)
        self.assertEqual(word.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()
