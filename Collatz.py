#!/usr/bin/env python3

# ------------------
# collatz/Collatz.py
# Copyright (C) 2018
# Glenn P. Downing
# ------------------
"""
prints and evalates collatz cycle length
"""
from typing import IO, List

# ------------
# collatz_read
# ------------
MY_CACHE = {1:1}

def collatz_read(word: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    save = word.split()
    return [int(save[0]), int(save[1])]

# ------------
# collatz_eval
# ------------


# -------------
# dictionary
# -------------

MY_CACHE = {1:1}


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    assert j > 0 and j < 1000000 # pre
    assert i > 0 and i < 1000000
    initial = i
    high = j
    max_count = 1
    greatest_val = j//2 + 1 #cuts down the range of the inputs 
    if i > j:
        initial = j
        high = i
    if i < greatest_val:
        initial = greatest_val
        high = j
    for number in range(initial, high + 1):
        count = 1 # starts at 1 because cycle length of 1 is 1
        if number in MY_CACHE:
            count = MY_CACHE[number]
        else:
            value = number
            loop = True
            while loop and value > 1:
                if value in MY_CACHE:
                    count += MY_CACHE[value]-1
                    loop = False
                    break
                elif value % 2 == 0:
                    value = value >> 1
                    count += 1
                else:
                    value = value + (value>>1) + 1
                    count += 2
        MY_CACHE[number] = count # updates dict with cycle number : cycle length
        max_count = max(max_count, count)
        assert max_count > 0 # post
    return max_count



# -------------
# collatz_print
# -------------

def collatz_print(writ_in: IO[str], number: int, value: int, val: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    writ_in.write(str(number) + " " + str(value) + " " + str(val) + "\n")

# -------------
# collatz_solve
# -------------

def collatz_solve(reading_in: IO[str], writing_in: IO[str]) -> None:
    """
    hello
    """
    for store_input in reading_in:
        i, j = collatz_read(store_input)
        value_store = collatz_eval(i, j)
        collatz_print(writing_in, i, j, value_store)
